const dbClient = require("./db");

const expenseService = () => {
  const getAll = async () => {
    let res = await dbClient().query("SELECT * FROM expense");
    if (res.error) {
      return { error: "error" };
    } else {
      return {
        expenses: res.result,
      };
    }
  };

  const create = async (expenseRequest) => {
    const insertStatement = generateInsertStatement(expenseRequest);
    let res = await dbClient().query(insertStatement);
    if (res.error) {
      return { error: "error" };
    } else {
      return { created: true };
    }
  };

  const deleteExpense = async (id) => {
    let res = await dbClient().query(`DELETE FROM expense WHERE id = ${id}`);
    if (res.error) {
      return { error: "error" };
    } else {
      return {
        deleted: true,
      };
    }
  };

  const generateInsertStatement = (expense) => {
    return `INSERT INTO expense VALUES (DEFAULT, '${expense.title}', ${expense.price}, '${expense.dateOccurred}', '${expense.category}')`;
  };

  return {
    getAll,
    create,
    deleteExpense,
  };
};

module.exports = expenseService;
