class Expense {
  constructor(title, price, dateOccurred, category) {
    this.title = title;
    this.price = price;
    this.dateOccurred = dateOccurred;
    this.category = category;
  }
}

module.exports = Expense;
