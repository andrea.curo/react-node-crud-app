const pgp = require("pg-promise")();
const DB_USER = process.env.REACT_APP_DB_USER || "";
const DB_USER_PW = process.env.REACT_APP_DB_USER_PW || "";
const DB_HOST = process.env.REACT_APP_DB_HOST || "";
const DB_PORT = process.env.REACT_APP_DB_PORT || "";
const DB_NAME = process.env.REACT_APP_DB_NAME || "";
const connectionString = `postgres://${DB_USER}:${DB_USER_PW}@${DB_HOST}:${DB_PORT}/${DB_NAME}`;
const db = pgp(connectionString);

const dbClient = () => {
  let sco;
  const query = async (queryStr) => {
    return await db
      .connect()
      .then((obj) => {
        sco = obj;
        return sco.any(queryStr);
      })
      .then((obj) => {
        return {
          result: obj,
        };
      })
      .catch((err) => {
        console.log("Failure accessing DB");
        return {
          error: err,
        };
      })
      .finally(() => {
        if (sco) {
          sco.done();
        }
      });
  };

  return {
    query,
  };
};

module.exports = dbClient;
