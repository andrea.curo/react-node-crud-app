const express = require("express");
const bodyParser = require("body-parser");
const pino = require("express-pino-logger")();
const cors = require("cors");
const Expense = require("./expenseModel");

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.json());
app.use(pino);
app.use(cors());
app.options("*", cors());

const expenseService = require("./service");

app.get("/api/expenses", async (req, res) => {
  const resBody = await expenseService().getAll();
  const resStatus = resBody.error ? 500 : 200;
  res.setHeader("Content-Type", "application/json");
  res.status(resStatus).send(resBody);
});

app.post("/api/expenses", async (req, res) => {
  // TODO: validate request
  if (
    req.body &&
    req.body.title &&
    req.body.price &&
    req.body.dateOccurred &&
    req.body.category
  ) {
    const expenseRequest = new Expense();
    expenseRequest.title = req.body.title;
    expenseRequest.price = req.body.price;
    expenseRequest.dateOccurred = req.body.dateOccurred;
    expenseRequest.category = req.body.category;

    const resBody = await expenseService().create(expenseRequest);
    const resStatus = resBody.error ? 400 : 200;
    res.setHeader("Content-Type", "application/json");
    res.status(resStatus).send(resBody);
  } else {
    res.status(400).send({ error: "Request body was malformed" });
  }
});

app.delete("/api/expenses/:id", async (req, res) => {
  const id = req.params.id;
  if (id) {
    const resBody = await expenseService().deleteExpense(id);
    const resStatus = resBody.error ? 400 : 200;
    res.setHeader("Content-Type", "application/json");
    res.status(resStatus).send(resBody);
  } else {
    res.status(400).send({ error: "Request body was malformed" });
  }
});

app.listen(3001, () =>
  console.log("Express server is running on localhost:3001")
);
