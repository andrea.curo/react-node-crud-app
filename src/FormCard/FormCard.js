import React from "react";

import { formCardService } from "./FormCardService";
import "./FormCard.css";
import constants from "../Utilities/Constants";

import {
  Card,
  TextField,
  Button,
  Select,
  InputLabel,
  MenuItem,
  FormControl,
} from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";

export default class FormCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        title: "",
        dateOccurred: null, // set to Null to avoid validation error with date picker
        price: "",
        category: "",
      },
    };
    this.formCardService = formCardService();
    this.constants = constants();

    this.addCard = this.addCard.bind(this);
  }

  async addCard() {
    await this.formCardService.create(this.state.form).then(async (res) => {
      if (res.error) {
        console.log(res.error);
      } else {
        this.setState({
          form: {
            title: "",
            dateOccurred: null,
            price: "",
            category: [],
          },
        });
        await this.props.callback();
      }
    });
  }

  handleChange(prop, event) {
    let tmpForm = this.state.form;
    if (prop === "dateOccurred") {
      tmpForm[prop] = event.toLocaleDateString();
    } else {
      tmpForm[prop] = event.target.value;
    }
    this.setState(() => ({
      form: tmpForm,
    }));
  }

  handleDelete(item) {
    let tmpForm = this.state.form;
    const index = tmpForm.category.indexOf(item);
    tmpForm.category.splice(index, 1);
    this.setState(() => ({
      form: tmpForm,
    }));
  }

  render() {
    return (
      <Card className="add-card">
        <form className="form" noValidate autoComplete="off">
          <TextField
            label="Expense"
            fullWidth
            value={this.state.form.title}
            onChange={(e) => this.handleChange("title", e)}
          />
          <TextField
            margin="normal"
            classes={{ root: "half-input" }}
            label="Price"
            type="Number"
            value={this.state.form.price}
            onChange={(e) => this.handleChange("price", e)}
          />
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker
              classes={{ root: "half-input" }}
              margin="normal"
              id="date-picker-dialog"
              format="MM/dd/yyyy"
              label="Date of Expense"
              value={this.state.form.dateOccurred}
              onChange={(e) => this.handleChange("dateOccurred", e)}
              KeyboardButtonProps={{
                "aria-label": "change date",
              }}
            />
          </MuiPickersUtilsProvider>
          <FormControl fullWidth>
            <InputLabel id="category-label">Category</InputLabel>
            <Select
              labelId="category-label"
              id="category"
              value={this.state.form.category}
              onChange={(e) => this.handleChange("category", e)}
            >
              {this.constants.category.map((name) => (
                <MenuItem key={name} value={name}>
                  {name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <Button
            className="add-card-btn"
            fullWidth
            variant="contained"
            color="primary"
            onClick={this.addCard}
          >
            Add Expense
          </Button>
        </form>
      </Card>
    );
  }
}
