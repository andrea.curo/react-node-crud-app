export const BASE_PATH = "localhost:3001";

export const formCardService = () => {
  const create = async (expense) => {
    const request = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(expense),
    };
    return fetch(`http://${BASE_PATH}/api/expenses`, request)
      .then((res) => res.json())
      .catch(function (err) {
        console.log(`Error on api call ${err}`);
        return {
          error: err,
        };
      });
  };

  return {
    create,
  };
};
