import React from "react";
import SimpleCard from "../SimpleCard/SimpleCard";
import FormCard from "../FormCard/FormCard";
import { cardWrapperService } from "./CardWrapperService";
import Overview from "../Overview/Overview";
import "./CardWrapper.css";

export default class CardWrapper extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cards: [],
    };
    this.cardWrapperService = cardWrapperService();

    this.getExpenses = this.getExpenses.bind(this);
    this.deleteCard = this.deleteCard.bind(this);
  }

  async getExpenses() {
    await this.cardWrapperService.getExpenses().then((result) => {
      if (result.error) {
        this.setState({
          cards: [],
        });
      } else {
        this.setState((state) => ({
          cards: result.expenses,
        }));
      }
    });
  }

  async deleteCard(id) {
    await this.cardWrapperService.deleteExpense(id).then(async (res) => {
      if (res.error) {
        console.log(res.error);
      } else {
        await this.getExpenses();
      }
    });
  }

  async componentDidMount() {
    this.getExpenses();
  }

  render() {
    return (
      <div className="app-content">
        <Overview cards={this.state.cards} />
        <div className="wrapper">
          <div className="card-wrapper">
            {this.state.cards.map((value, index) => {
              return (
                <SimpleCard key={index} info={value} delete={this.deleteCard} />
              );
            })}
          </div>
          <div>
            <FormCard callback={this.getExpenses}></FormCard>
          </div>
        </div>
      </div>
    );
  }
}
