export const BASE_PATH = "localhost:3001";

export const cardWrapperService = () => {
  const getExpenses = async () => {
    return fetch(`http://${BASE_PATH}/api/expenses`)
      .then((res) => res.json())
      .catch((err) => {
        console.log(`Error on api call ${err}`);
        return {
          error: err,
        };
      });
  };

  const deleteExpense = async (id) => {
    return fetch(`http://${BASE_PATH}/api/expenses/${id}`, {
      method: "DELETE",
    })
      .then((res) => res.json())
      .catch((err) => {
        console.log(`Error on api call ${err}`);
        return {
          error: err,
        };
      });
  };

  return {
    getExpenses,
    deleteExpense,
  };
};
