import React from "react";

import { Card, CardHeader, CardContent, Typography } from "@material-ui/core";
import { PieChart } from "react-minimal-pie-chart";
import constants from "../Utilities/Constants";
import Currency from "../Utilities/Currency";
import "./Overview.css";

export default class Overview extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: {},
      total: 0,
      pieData: [],
    };

    this.constants = constants();
  }

  calculateData() {
    const cards = this.props.cards;
    const data = cards.reduce((groupedObj, current) => {
      const price = Number.parseFloat(current.price);
      if (groupedObj[current.category]) {
        groupedObj[current.category] += price;
      } else {
        groupedObj[current.category] = price;
      }
      return groupedObj;
    }, {});

    const total = Object.keys(data).reduce((sum, key) => {
      return sum + data[key];
    }, 0);

    const pieData = Object.keys(data).map((key) => {
      return {
        title: key,
        value: data[key],
        color: this.constants.colorMap[key] + "",
      };
    });
    this.setState((state) => ({ data: data, total: total, pieData: pieData }));
  }

  capitalize(str) {
    return str.substring(0, 1).toUpperCase() + str.substring(1);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.cards.length !== this.props.cards.length) {
      this.calculateData();
    }
  }

  render() {
    return (
      <div className="overview-wrapper">
        <Card classes={{ root: "overview-card" }}>
          <CardHeader title="Overview"></CardHeader>
          <CardContent>
            <Typography variant="h6" color="primary" gutterBottom>
              Accounts
              <span className="float-right">
                <Currency amt="3000.00"></Currency>
              </span>
            </Typography>
            <Typography variant="h6" color="primary" gutterBottom>
              Income
              <span className="float-right">
                <Currency amt="1500.00"></Currency>
              </span>
            </Typography>
            <Typography variant="h6" color="secondary" gutterBottom>
              Total Expenses
              <span className="float-right">
                <Currency amt={this.state.total}></Currency>
              </span>
            </Typography>
            {Object.keys(this.state.data).map((key) => {
              return (
                <Typography
                  variant="body1"
                  color="secondary"
                  gutterBottom
                  key={key}
                >
                  <span>{this.capitalize(key)}</span>
                  <span className="float-right">
                    <Currency amt={this.state.data[key]}></Currency>
                  </span>
                </Typography>
              );
            })}
          </CardContent>
        </Card>
        <Card classes={{ root: "overview-category" }}>
          <CardHeader title="Category"></CardHeader>
          <CardContent>
            <div className="pie-wrapper">
              <div className="pie">
                <PieChart
                  labelStyle={() => {
                    return { fontSize: "0.5em" };
                  }}
                  label={({ dataEntry }) =>
                    `${Math.round(dataEntry.percentage)} %`
                  }
                  data={this.state.pieData}
                />
              </div>
              <div className="legend">
                {Object.keys(this.constants.colorMap).map((key) => (
                  <div key={key}>
                    <svg width="10" height="10">
                      <circle
                        cx="5"
                        cy="5"
                        r="5"
                        fill={this.constants.colorMap[key]}
                      />
                    </svg>
                    <span>&nbsp;{key}</span>
                  </div>
                ))}
              </div>
            </div>
          </CardContent>
        </Card>
      </div>
    );
  }
}
