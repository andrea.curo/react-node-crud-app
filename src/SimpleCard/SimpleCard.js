import React from "react";

import Chip from "@material-ui/core/Chip";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import DeleteRounded from "@material-ui/icons/DeleteRounded";
import "./SimpleCard.css";

export default class SimpleCard extends React.Component {
  render() {
    return (
      <Card className="root">
        <CardHeader
          title={this.props.info.title + " - $" + this.props.info.price}
          subheader={this.props.info.dateoccurred}
        ></CardHeader>
        <CardContent classes={{ root: "sc-card-content" }}>
          <Chip
            color="secondary"
            label={this.props.info.category}
            classes={{ root: "chip" }}
            size="small"
          />
        </CardContent>
        <CardActions disableSpacing>
          <IconButton
            aria-label="add to favorites"
            color="primary"
            classes={{ root: "sc-icon-right" }}
            onClick={() => this.props.delete(this.props.info.id)}
          >
            <DeleteRounded />
          </IconButton>
        </CardActions>
      </Card>
    );
  }
}
