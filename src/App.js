import React from "react";

import { AppBar, Toolbar, Typography } from "@material-ui/core";
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import { amber, teal } from "@material-ui/core/colors";

import CardWrapper from "./CardWrapper/CardWrapper";
import "./App.css";

const theme = createMuiTheme({
  fontSize: 12,
  palette: {
    primary: {
      main: teal[500],
      contrastText: "#fff",
    },
    secondary: {
      main: amber[500],
      contrastText: "#fff",
    },
  },
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <div className="app">
        <AppBar position="static" color="primary">
          <Toolbar>
            <Typography variant="h4" color="inherit">
              Expenses
            </Typography>
          </Toolbar>
        </AppBar>
        <CardWrapper />
      </div>
    </ThemeProvider>
  );
}

export default App;
