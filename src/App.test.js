import React from "react";
import { render } from "@testing-library/react";
import App from "./App";

test("renders add card button", () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/Add Expenses/i);
  expect(linkElement).toBeInTheDocument();
});

test("tests are valid", () => {
  const { queryByText } = render(<App />);
  const linkElement = queryByText(/lorem ipsum/i);
  expect(linkElement).toBeNull();
});
