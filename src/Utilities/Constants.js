import { purple, lime, orange, lightBlue } from "@material-ui/core/colors";

const constants = () => {
  const colorMap = {
    food: orange[400],
    household: lightBlue[400],
    entertainment: lime[400],
    miscellaneous: purple[400],
    subscriptions: lime[600],
    school: orange[200],
  };

  const category = Object.keys(colorMap);

  return {
    category,
    colorMap,
  };
};

export default constants;
