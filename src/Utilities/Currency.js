import React from "react";

export default class Currency extends React.Component {
  format(input) {
    if (typeof input === "string") {
      return Number.parseFloat(input).toFixed(2);
    } else if (typeof input === "number") {
      return input.toFixed(2);
    }
  }

  render() {
    return <span>${this.format(this.props.amt)}</span>;
  }
}
