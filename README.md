# Basic CRUD React + Node app
Simple app to track expenses

## Made With
- [ReactJS](https://reactjs.org/)
- [Node/Express](https://nodejs.org/en/)
- [Material-UI](https://material-ui.com/)
- [react-minimal-pie-chart](https://github.com/toomuchdesign/react-minimal-pie-chart)

## Start

To start the server and web app

>  `npm run-scripts dev`

This command will start both frontend and backend services


## ToDo
- Use React Hooks for API interaction
- Implement more sophisticated status code
- Add form validation
- Add currency option

